var express = require('express')
var config = require('./config/index')

var port = process.env.PORT || config.build.port

var app = express()

var router = express.Router()

router.get('/', function (req, res, next) {
  req.url = '/index.html'
  next()
})

app.use(router)

// 定义接收对象
var appData = require('./data.json')
var seller = appData.seller
var goods = appData.goods
var ratings = appData.ratings

// 定义路由
var apiRoutes = express.Router()
// 请求seller时
apiRoutes.get('/seller', function (req, res) {
  res.json({
    errno: 0, /* 表示请求数据是正常的，根据业务方自定义的'错误码' */
    data: seller
  })
})
// 请求goods时
apiRoutes.get('/goods', function (req, res) {
  res.json({
    errno: 0,
    data: goods
  })
})
// 请求评论ratings时
apiRoutes.get('/ratings', function (req, res) {
  res.json({
    errno: 0,
    data: ratings
  })
})
// use(path, )，接口相关的api都会先通过/api路由，然后再访问我们的api
app.use('/api', apiRoutes)

app.use(express.static('./dist'))

module.exports = app.listen(port, function (err) {
  if (err) {
    console.log(err)
    return
  }
  console.log('Listening at http://localhost:' + port + '\n')
})

