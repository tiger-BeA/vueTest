// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import App from './App'
import Goods from 'components/goods/goods'
import Ratings from 'components/ratings/ratings'
import Seller from 'components/seller/seller'

import 'common/stylus/index.styl'

/* 相当于全局注册 */
Vue.use(VueRouter)
Vue.use(VueResource)

/* 创建一个路由器实例
 * 定义路由规则
 * 每一条路由规则应该映射到一个组件
 * 这里的组件可以是一个使用Vue.extend创建的组件构造函数
 * 也可以是一个组件选项对象
 */

let router = new VueRouter({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/goods',
      component: Goods
    },
    {
      path: '/ratings',
      component: Ratings
    },
    {
      path: '/seller',
      component: Seller
    }
  ]
})

// 路由器需要一个根组件
/* eslint-disable no-new */
new Vue({
  router,
  template: '<App/>',
  components: { App }
}).$mount('#app')

router.push('/goods')

